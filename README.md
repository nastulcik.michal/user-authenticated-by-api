
# Oxyshop - modelove zadani

Projekt je založen na volání všech potřebných dat z API, nikoliv rovnou z controlleru.

Projekt obsahuje přihlášení, registraci a zobrazení všech uživatelů registrovaných uživatelů

Projekt obsahuje dva oddělené frontend celky, které vykonávájí tutéž práci, ale je zde
využito jiných technologií.

1 - Vue.js ₊ axios.

2 - Klasický twig + axios + symfony formulář

Druhá část aplikace je REST API.

Frontend jsem vyhotovil záměrně v těchto dvou technologiích, kvůli touze vyzkoušet vue.js a zároveň jsem se znažil dodržet modelové zadání.

Určitě se najde něco co lze dělat lépe a efektivnějí, ovšem zde byl hlavní záměr zprostředkovat funkční řešení. Zároveň
bych ještě rád dodal, že co se týče symfony tak je to moje prvotina :) . Opravdu byla radost na tomto projektu pracovat a načerpat tím nové a velmi užitečné zkušenosti.
Budu velmi rád za jakkoukoliv konstruktivní kritiku :)

Technologie:

PHP 7.4 + SQLite DB + yarn

## Installation

Clone the project

```bash
git clone https://gitlab.com/nastulcik.michal/oxyshop-modelove-zadani
```

Go to the project directory

```bash
cd oxyshop-modelove-zadani
```

Install dependencies

```bash
composer install
```
```bash
yarn install
```
```bash
yarn build
```

Prepare database 

```bash
symfony console doctrine:migrations:migrate
```

## Run Locally


Start the server

```bash
symfony serve -d
```

For make changes in assets:
```bash
yarn watch
```


## Features

- Login, Registration and show user list

Now you can go to http://127.0.0.1:8000/

<!-- yarn add vue@^2.5 vue-loader@^15.9.5 vue-template-compiler
yarn add axios -->