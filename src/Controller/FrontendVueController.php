<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/vue")
 */
class FrontendVueController extends AbstractController
{
    /**
     * @Route("/registration", name="app_registration_vue")
     */
    public function registration(): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('app_profile_vue');
        }

        return $this->render('frontend/vue/registration.html.twig', [
            'controller_name' => 'FrontendVueController',
        ]);
    }
    
    /**
     * @Route("/login", name="app_login_vue")
     */
    public function login(): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('app_profile_vue');
        }
        return $this->render('frontend/vue/login.html.twig', [
            'controller_name' => 'FrontendVueController',
        ]);
    }

    /**
     * @Route("/user-list", name="app_user_list_vue")
     */
    public function userList(): Response
    {
        return $this->render('frontend/vue/user_list.html.twig');
    }
}
