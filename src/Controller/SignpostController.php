<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SignpostController extends AbstractController
{
    /**
     * @Route("/", name="app_signpost")
     */
    public function index(): Response
    {
        return $this->render('frontend/signpost/index.html.twig');
    }
}
