<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/twig")
 */
class FrontendTwigController extends AbstractController
{
    /**
     * @Route("/registration", name="app_registration_twig")
     */
    public function registration(): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('app_profile_twig');
        }

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        return $this->renderForm('frontend/twig/registration.html.twig', [
            'form' => $form,
        ]);
    }
    
    /**
     * @Route("/login", name="app_login_twig")
     */
    public function login(): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('app_profile_twig');
        }
        return $this->render('frontend/twig/login.html.twig', [
            'controller_name' => 'FrontendVueController',
        ]);
    }

    /**
     * @Route("/user-list", name="app_user_list_twig")
     */
    public function userList(): Response
    {
        return $this->render('frontend/twig/user_list.html.twig');
    }
}
