import { Controller } from '@hotwired/stimulus';
import axios from 'axios';

export default class extends Controller {
    static targets = ['error', 'email', 'password'];

    connect() {
        this.errorTarget.style.display = 'none';
    }

    login() {
        axios
            .post('/login', {
                email: this.emailTarget.value,
                password: this.passwordTarget.value
            }, {
                headers: {
                    'Contact-Type' : 'application/x-www-form-urlencoded'
                }
            })
            .then(response => {
                if (response.status === 204) {
                    window.location = "/twig/profile"; 
                    return;
                }

                this.errorTarget.innerHTML = 'Unkown errror. Please try it later';
                this.errorTarget.style.display = 'block';

            }).catch(error => {
                this.errorTarget.innerHTML = 'Unkown errror. Please try it later';
                this.errorTarget.style.display = 'block';

                if (error.response) {
                     if (error.response.data) {
                        this.errorTarget.innerHTML = error.response.data.error;
                    }
                }
            })
    }
}
