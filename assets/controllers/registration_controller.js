import { Controller } from '@hotwired/stimulus';
import axios from 'axios';

export default class extends Controller {
    static targets = ['error', 'success', 'email', 'password', 'username', 'admin'];

    connect() {
        this.errorTarget.style.display = 'none';
        this.successTarget.style.display = 'none';
    }

    registrate() {
        var user_roles = [];

        if (this.adminTarget.value === 1) {
            user_roles = ['ROLE_ADMIN'];
        }

        axios
            .post('/api/users', {
                email: this.emailTarget.value,
                password: this.passwordTarget.value,
                username: this.usernameTarget.value,
                roles: user_roles
            }, {
                headers: {
                    'Contact-Type': 'application/x-www-form-urlencoded'
                }
            })
            .then(response => {
                if (response.data.email === this.emailTarget.value) {
                    this.showSuccess('You are succesfully registrated. Try log in');
                } else {
                    this.showError('Unkown errror. Please try it later');
                }
            }).catch(error => {
                this.showError(this.getErrorMsg(error));
            })
    }

    showSuccess(text) {
        this.successTarget.style.display = 'block';
        this.errorTarget.style.display = 'none';
        this.successTarget.innerHTML = text;
    }

    showError(text) {
        this.errorTarget.style.display = 'block';
        this.successTarget.style.display = 'none';
        this.errorTarget.innerHTML = text;
    }

    getErrorMsg(error) {
        var error_msg = 'Unkown errror. Please try it later';

        if (!error.response.data || !error.response.data.violations) {
            return error_msg;
        }
        
        var violations = error.response.data.violations;

        if (!violations || !violations[0] || !violations[0].message) {
            return error_msg;
        }

        return error.response.data.violations[0].message;
    }
}
