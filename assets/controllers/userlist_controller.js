import { Controller } from '@hotwired/stimulus';
import axios from 'axios';

export default class extends Controller {
    static targets = ['error', 'list'];

    connect() {
        axios
            .get('/api/users.json')
            .then(response => (
                this.renderList(response)
            )).catch(error => {
                this.errorTarget.style.display = 'block';
                this.errorTarget.innerHTML = this.getErrorMsg(error);
            })
    }

    getErrorMsg(error) {
        var error_msg = 'Unkown errror. Please try it later';

        if (!error.response.data || !error.response.data.violations) {
            return error_msg;
        }
        
        var violations = error.response.data.violations;

        if (!violations || !violations[0] || !violations[0].message) {
            return error_msg;
        }

        return error.response.data.violations[0].message;
    }

    renderList(response) {
        console.log(response);

        var tbl = document.createElement("table");
        tbl.setAttribute("class", "table");


        tbl.appendChild(this.createTableHeader(['Username','Email', 'Roles']));
        var tblBody = document.createElement("tbody");

        response.data.forEach(user => {
            tblBody.appendChild(this.createRow(user));
        });

        tbl.appendChild(tblBody);
        this.listTarget.appendChild(tbl);
    }

    createTableHeader(th_list) {
        var thead = document.createElement("thead");
        th_list.forEach(cell_name => {
            thead.appendChild(this.createThCell(cell_name));
        });
        return thead;
    }

    createRow(user) {
        var row = document.createElement("tr");
        row.appendChild(this.createCell(user.email));
        row.appendChild(this.createCell(user.username));
        row.appendChild(this.createCell(user.roles.join(", ")));
        return row;
    }

    createThCell(text) {
        return this.createElementWithText("th", text);
    }

    createCell(text) {
        return this.createElementWithText("td", text);
    }

    createElementWithText(element, text) {
        var element = document.createElement(element);
        var textNode = document.createTextNode(text);
        element.appendChild(textNode);
        return element;
    }

}
