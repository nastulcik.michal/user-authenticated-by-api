/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import 'bootstrap/dist/css/bootstrap.css';


const $ = require('jquery');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import Vue from 'vue';

// start the Stimulus application
import './bootstrap';

import UserList from './components/UserList';
import RegistrationForm from './components/RegistrationForm';
import LoginForm from './components/LoginForm';

Vue.component('user-registration-app', RegistrationForm);
Vue.component('user-list-app', UserList);
Vue.component('user-login-app', LoginForm);


if (document.querySelector('#user-registration-app')) {
    const app = new Vue({
        el: '#user-registration-app',
    });
}

if (document.querySelector('#user-list-app')) {
    const app = new Vue({
        el: '#user-list-app'
    });
}

if (document.querySelector('#user-login-app')) {
    const app = new Vue({
        el: '#user-login-app',
    });
}